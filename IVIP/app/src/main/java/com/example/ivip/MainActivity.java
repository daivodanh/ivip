package com.example.ivip;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button btn_phone,btn_facebook;
    private static int My_request_code =100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        btn_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this,LoginByPhone.class);
                startActivityForResult(intent,My_request_code);
            }
        });
    }
    public void Anhxa(){
        btn_phone = findViewById(R.id.btn_phone);
        btn_facebook = findViewById(R.id.btn_facebook);
    }

}
